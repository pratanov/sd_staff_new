<?php
use Tygh\Registry;

defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD']  == 'POST') {
    fn_trusted_vars('members', 'member_data', 'member', 'result');
    $suffix = '';

    if ($mode == 'update') {
        $member_id = fn_sd_staff_update_member($_REQUEST['member_data'], $_REQUEST['member_data']['staff_id']);;

        $suffix = '.update?staff_id='.$member_id;
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['staff_id'])) {
            fn_sd_staff_delete_member($_REQUEST['staff_id']);
        }

        $suffix = '.manage';
    }

    return array(CONTROLLER_STATUS_OK, 'sd_staff' . $suffix);
}

if ($mode == 'manage') {
    list($members, $params) = fn_sd_staff_get_members($_REQUEST, DESCR_SL, Registry::get('settings.Appearance.admin_elements_per_page'));
    Tygh::$app['view']->assign(array('members' => $members,  'search' => $params));
} elseif ($mode == 'update') {
    $member = fn_sd_staff_get_member($_REQUEST['staff_id']);    

    if (empty($member)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Tygh::$app['view']->assign('member', $member);
}
