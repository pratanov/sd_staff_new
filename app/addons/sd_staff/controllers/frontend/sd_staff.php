<?php
defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD']  == 'POST') {
    fn_trusted_vars('staff_id');
    $suffix = '';

    if ($mode == 'sd_staff_get_member_mail') {
        $staff_id = isset($_REQUEST['staff_id']) ? $_REQUEST['staff_id'] : null;
        $suffix = '.sd_staff_get_member_mail?staff_id=' . $staff_id;
    }

    return array(CONTROLLER_STATUS_OK, 'sd_staff'. $suffix);
}

if ($mode =='sd_staff_get_member_mail') {
    if (isset($_REQUEST['staff_id'])) {
        $member_mail = fn_sd_staff_get_member_mail($_REQUEST['staff_id']);
        Tygh::$app['view']->assign('member_mail', $member_mail);
        Tygh::$app['view']->display('addons/sd_staff/blocks/sd_staff.tpl');
    }

    return false;
}
