<?php
defined('BOOTSTRAP') or die('Access denied');

function fn_sd_staff_get_members($params = array(), $lang_code = CART_LANGUAGE, $items_per_page = 0)
{

    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sortings = array(
        'staff_id' => '?:sd_staff.staff_id',
        'position' => '?:sd_staff.position',
    );

    if (AREA == 'C') {
        $sortby = 'position';
    } else if (AREA == 'A') {
        $sortby = 'staff_id';
    }

    $sorting = db_sort($params, $sortings, $sortby, 'asc');

    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:sd_staff WHERE 1");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $staff_members_list = db_get_hash_array('SELECT * FROM ?:sd_staff ?p ?p', 'staff_id', $sorting, $limit);

       foreach ($staff_members_list as $key => $staff_member) {
        if ($staff_member['user_id'] > 0) {
            $additional_data = db_get_row('SELECT firstname, lastname, email FROM ?:users WHERE user_id = ?i', $staff_member['user_id']);
        }

        if (!empty($additional_data)) {
            $staff_members_list[$key]['name'] = $additional_data['firstname'];
            $staff_members_list[$key]['lastname'] = $additional_data['lastname'];
            $staff_members_list[$key]['email'] = $additional_data['email'];
            $additional_data = false;
        }
        $staff_members_list[$key]['main_pair'] = fn_get_image_pairs($staff_member['staff_id'], 'staff', 'M', 'sd_staff');        
    }

        return array($staff_members_list, $params);

    return false;
}

function fn_sd_staff_get_member_mail($member_id)
{
    if (intval($member_id) > 0) {
        $member_mail = db_get_field('SELECT email FROM ?:sd_staff WHERE staff_id = ?i', $member_id);

        if (isset($member_mail) && ($member_mail != '')) {
            return $member_mail;
        } else {
            $user_id = db_get_field('SELECT user_id FROM ?:sd_staff WHERE staff_id = ?i', $member_id);
            $member_mail = db_get_field('SELECT email FROM ?:users WHERE user_id = ?i', $user_id);
            return $member_mail;
        }
    }

    return false;
}

function fn_sd_staff_get_member($member_id)
{
    if (intval($member_id) > 0) {
        $member = db_get_row('SELECT * FROM ?:sd_staff WHERE staff_id = ?i', $member_id);

        $member['main_pair'] = fn_get_image_pairs($member['staff_id'], 'staff', 'M', 'sd_staff');    
        return $member;
    }

    return false;
}

function fn_sd_staff_delete_member($member_id)
{
    if (isset($member_id) && intval($member_id) > 0) {
        $is_member_delete = db_query('DELETE FROM ?:sd_staff WHERE staff_id = ?i', $member_id);
    
        if ($is_member_delete) {
            fn_delete_image_pairs($_REQUEST['staff_id'], 'staff', 'sd_staff');
        }
        
        return $is_member_delete;
    }
    
    return false;
}

function fn_sd_staff_update_member($member_data, $member_id)
{
    if (isset($member_data)) {
        $member_data['user_id'] = fn_sd_staff_user_available_check($member_data['user_id']);

        if ($member_data['staff_id'] > 0) {
            $member = db_query('UPDATE ?:sd_staff SET ?u WHERE staff_id = ?i;', $member_data, $member_data['staff_id']);
        } else {
            $member_data['staff_id'] = db_query('INSERT INTO ?:sd_staff ?e', $member_data);  
        }

        fn_attach_image_pairs('staff_image', 'staff', $member_data['staff_id'], 'sd_staff');
    
        return $member_data['staff_id'];
    }

    return false;
}

function fn_sd_staff_user_available_check($user_id)
{
    if (intval($user_id > 0)) {
        $is_user_exist = db_get_row('SELECT * FROM ?:users WHERE user_id = ?i', $user_id);
        $user_id = empty($is_user_exist) ? 0 : $user_id;

        return $user_id;
    }

    return false;
}
