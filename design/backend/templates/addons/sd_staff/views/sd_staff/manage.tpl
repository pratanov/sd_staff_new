{** members section **}
{capture name="mainbox"}
    <form action="{""|fn_url}" method="post" name="members_form" class=" cm-hide-inputs" enctype="multipart/form-data">
    <input type="hidden" name="fake" value="1" />
    {include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id="pagination_contents_members"}

    {$c_url = $config.current_url|fn_query_remove:"sort_by":"sort_order"}
    {$rev = $smarty.request.content_id|default:"pagination_contents_members"}
    {$c_dummy = "<i class=\"icon-dummy\"></i>"}

    {if $members}
        <div class="table-responsive-wrapper">
            <table class="table table-middle table-responsive">
                <thead>
                <tr>
                <th><a class="cm-ajax" href="{"{$c_url}&sort_by=name&sort_order={$search.sort_order_rev}"|fn_url}" data-ca-target-id={$rev}>{__("member")}{if $search.sort_by == "name"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"{$c_url}&sort_by=type&sort_order={$search.sort_order_rev}"|fn_url}" data-ca-target-id={$rev}>{__("staff_function")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"{$c_url}&sort_by=type&sort_order={$search.sort_order_rev}"|fn_url}" data-ca-target-id={$rev}>{__("email")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                <th class="mobile-hide"><a class="cm-ajax" href="{"{$c_url}&sort_by=type&sort_order={$search.sort_order_rev}"|fn_url}" data-ca-target-id={$rev}>{__("user")}{if $search.sort_by == "type"}{$c_icon nofilter}{else}{$c_dummy nofilter}{/if}</a></th>
                {hook name="members:manage_header"}
                {/hook}
                <th width="6%" class="mobile-hide">&nbsp;</th>
                </tr>
                </thead>
                {foreach $members as $member}
                    <tr>
                        {$allow_save = $member|fn_allow_save_object:"members"}
                        {if $allow_save}
                            {$no_hide_input = "cm-no-hide-input"}
                        {else}
                            {$no_hide_input = ""}
                        {/if}
                        <td class="{$no_hide_input}" data-th="{__("member")}">
                            <a class="row-status" href="{"sd_staff.update?staff_id={$member.staff_id}"|fn_url}">{$member.name} {$member.lastname}</a>
                        </td>
                        <td class="nowrap row-status {$no_hide_input} mobile-hide">
                            {$member.function}
                        </td>
                        <td class="nowrap row-status {$no_hide_input} mobile-hide">
                            {$member.email}
                        </td>
                        <td class="nowrap row-status {$no_hide_input} mobile-hide">
                            {if $member.user_id}
                                <a class="row-status" href="{"profiles.update&user_id={$member.user_id}"|fn_url}">{__("edit")}</a>
                            {/if}
                        </td>
                        {hook name="members:manage_data"}
                        {/hook}
                        <td class="mobile-hide">
                            {capture name="tools_list"}
                                <li>{btn type="list" text=__("edit") href="sd_staff.update?staff_id={$member.staff_id}"}</li>
                                {if $allow_save}
                                    <li>{btn type="list" class="cm-confirm" text=__("delete") href="sd_staff.delete?staff_id={$member.staff_id}" method="POST"}</li>
                                {/if}
                            {/capture}
                            <div class="hidden-tools">
                                {dropdown content=$smarty.capture.tools_list}
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        </div>
    {else}
        <p class="no-items">{__("no_data")}</p>
    {/if}
    {include file="common/pagination.tpl" div_id="pagination_contents_members"}

    {capture name="adv_buttons"}
        {hook name="members:adv_buttons"}
            {include file="common/tools.tpl" tool_href="sd_staff.add" prefix="top" hide_tools="true" title=__("add_member") icon="icon-plus"}
        {/hook}
    {/capture}
    </form>
{/capture}

{hook name="members:manage_mainbox_params"}
    {$page_title = __("members")}
    {$select_languages = true}
{/hook}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=$select_languages sidebar=$smarty.capture.sidebar}

{** ad section **}