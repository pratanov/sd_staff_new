{** members section **}
{$allow_save = $member|fn_allow_save_object:"members"}
{capture name="mainbox"}
    {capture name="buttons"}
        {if !$member.staff_id}
            {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="members_form" but_name="dispatch[sd_staff.update]"}
        {else}
            {if "ULTIMATE"|fn_allowed_for && !$allow_save}
                {assign var="hide_first_button" value=true}
                {assign var="hide_second_button" value=true}
            {/if}
            {include file="buttons/save_cancel.tpl" but_name="dispatch[sd_staff.update]" but_role="submit-link" but_target_form="members_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$member.staff_id}
        {/if}
    {/capture}
    {if !$error}
        <form action="{""|fn_url}" method="post" enctype="multipart/form-data" class="form-horizontal form-edit cm-processed-form cm-check-changes" name="members_form">
            <input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
            {capture name="tabsbox"}
                <div id="content_general">
                    <input type="hidden" name="member_data[staff_id]" value="{$member.staff_id}" />
                    <div class="control-group">
                        <label for="elm_member_name" class="control-label cm-required">{__("firstname")}</label>
                        <div class="controls">
                            <input name="member_data[name]" type="text" id="elm_member_name" value="{$member.name}">
                        </div>		
                    </div>
                    <div class="control-group">
                        <label for="elm_member_lastname" class="control-label cm-required">{__("lastname")}</label>
                        <div class="controls">
                            <input name="member_data[lastname]" type="text" id="elm_member_lastname" value="{$member.lastname}" />
                        </div>
                    </div>
                    <div class="control-group">	
                        <label for="elm_member_email" class="control-label cm-required">{__("email")}</label>
                        <div class="controls">
                            <input name="member_data[email]" type="text" id="elm_member_email" value="{$member.email}" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="elm_member_function" class="control-label cm-required">{__("staff_function")}</label>
                        <div class="controls">
                            <textarea name="member_data[function]" id="elm_member_function" cols="50" rows="3">{$member.function}</textarea>
                        </div>
                    </div>
                    <div class="control-group">					
                        <label for="elm_member_photo" class="control-label cm-required">{__("photo")}</label>
                        <div class="controls">
                            {include file="common/attach_images.tpl"
                                image_name="staff_image"
                                image_object_type="staff"
                                image_pair=$member.main_pair
                                image_object_id=$member.staff_id
                                image_type="M"
                                no_detailed=true
                                hide_titles=true
                            }
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="elm_member_user" class="control-label cm-required">{__("user")}</label>
                        <div class="controls">
                            <input name="member_data[user_id]" type="text" value="{$member.user_id}" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="elm_member_position" class="control-label cm-required">{__("position")}</label>
                        <div class="controls">
                            <input name="member_data[position]" type="text" value="{$member.position}" />
                        </div>
                        {__("position_description")}
                    </div>			
                </div>
            {/capture}
            {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}
        </form>
    {else}
        {__("{$error}")}
    {/if}							
{/capture}

{hook name="members:manage_mainbox_params"}
    {if $member.staff_id}
         {$page_title = __("staff_member")}
    {else}
        {$page_title = __("add_member")}
    {/if}
    {$select_languages = true}
{/hook}



{include file="common/mainbox.tpl"
    title=$page_title
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons
    adv_buttons=$smarty.capture.adv_buttons
    select_languages=$select_languages
    sidebar=$smarty.capture.sidebar
}