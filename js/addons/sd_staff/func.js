(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#team-slider');
        var slider_block_width = $(slider).closest('div').width();
        var items_number = Math.floor(slider_block_width/130);

        if (slider.length) {
            slider.owlCarousel({
                items: items_number,
                dots: false,
                navigation: true,
                pagination: false,
                navigationText: ['', ''],
            });
        }
    });

    $(document).ready(function(){
        $('.sd_staff_view_mail').on('click', fn_sd_staff_get_mail);
    });

    function fn_sd_staff_get_mail(e) {
        e.preventDefault();
        var staff_id = $(this).attr('data-member-id');

        $.ceAjax('request', fn_url('sd_staff.sd_staff_get_member_mail'), {
            method: 'post',
            data: {
                staff_id: staff_id,
                result_ids: 'sd_staff_member_item_'+staff_id,
            },
        });
    }

}(Tygh, Tygh.$));