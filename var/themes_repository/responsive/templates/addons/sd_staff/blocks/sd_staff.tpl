{script src="js/addons/sd_staff/func.js"}
{if $items}
    <div class="team-slide owl-carousel">
        <ul id="team-slider">
            {foreach $items as $member}
                <li class="item team-slider-item">
                    {if $member.main_pair.icon.relative_path}
                        <img src="images/{$member.main_pair.icon.relative_path}" class="team-slider-img" alt="{$member.name} {$member.lastname}" /> 
                    {else}
                        <img src="images/no_image.png" class="team-slider-img" alt="{$member.name} {$member.lastname}" />                        
                    {/if}
                    <h4>{$member.name} {$member.lastname}</h4>
                    {$member.function}<br />
                    <div id="sd_staff_member_item_{$member.staff_id}">
                        {if $member_mail}
                            <a class="sd_staff_mailto" href="mailto:{$member_mail}">{$member_mail}</a>                        
                        {else}
                            <a class="sd_staff_view_mail" data-member-id="{$member.staff_id}">{__("view_email")}</a>
                        {/if}                        
                    <!--sd_staff_member_item_{$member.staff_id}--></div>
                </li>
            {/foreach}
       </ul>
    </div>
{/if}